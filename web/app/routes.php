<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\Actions\Expedition\ExpeditionGetAction;
use App\Application\Actions\Expedition\ExpeditionMoveAction;
use App\Application\Actions\Expedition\ExpeditionNewAction;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {

        //TODO:Redirect to front html page.
        $response->getBody()->write(file_get_contents(dirname(__FILE__) . '/../public/index.html'));
        return $response;
    });

    $app->group('/expedition', function (Group $group) {
        $group->post('/new', ExpeditionNewAction::class);
        $group->post('/', ExpeditionMoveAction::class);
        $group->get('/', ExpeditionGetAction::class);
    });
};

