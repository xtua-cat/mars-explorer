<?php

/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests\Domain;

use Tests\TestCaseBase;
use App\Domain\Planet\Planet;
use App\Domain\Robot\Robot;
use App\Domain\Expedition\Expedition;

/**
 * Description of PlanetTest
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class ExpeditionTest extends TestCaseBase {

    protected Planet $planet;
    protected array $robots = [];
    protected Expedition $expedition;

    public function setUp(): void {
        parent::setUp();

        $this->planet = new Planet('test', 10, 10);
        $this->planet->addRock(7, 7);
        $this->robots = [
            new Robot('robot0', 0, 0, Robot::DIRECTION_EAST),
            new Robot('robot1', 5, 7, Robot::DIRECTION_SOUTH)
        ];

        $this->expedition = new Expedition($this->planet);
        $this->expedition->addRobot($this->robots[0])
                ->addRobot($this->robots[1]);
    }

    public function testSendSameRobotException() {
        $this->expectException(\App\Domain\Expedition\RobotAlreadyInPlanetException::class);
        $this->expedition->addRobot($this->robots[0]);
    }

    public function testLandRobotInUnexistingCoordinates() {
        $this->expectException(\App\Domain\Expedition\RobotCannotLandInCoordinatesException::class);
        $this->expedition->addRobot(new Robot('cannotLandRobotName', 10, 10, Robot::DIRECTION_EAST));
    }

    public function testRobotNotExistsException() {
        $this->expectException(\App\Domain\Expedition\RobotDoesNotExistsException::class);
        $this->expedition->move('testNonExistingRobot', 'F');
    }

    public function testInvalidMovements() {
        $this->expectException(\App\Domain\Expedition\RobotCannotUnderstandException::class);
        $this->expedition->move($this->robots[0]->getName(), 'RT');
    }

    public function testSimpleMovement() {
        $this->expedition->move($this->robots[0]->getName(), 'R');
        $expeditionRobots = $this->expedition->getRobots();
        $expectedX = 0;
        $expectedY = 1;
        $expectedDirection = Robot::DIRECTION_SOUTH;

        $this->assertEquals($expectedX, $expeditionRobots[$this->robots[0]->getName()]->getPosX());
        $this->assertEquals($expectedY, $expeditionRobots[$this->robots[0]->getName()]->getPosY());
        $this->assertEquals($expectedDirection, $expeditionRobots[$this->robots[0]->getName()]->getDirection());
    }

    public function testSimpleInvalidMovement() {

        $this->assertEquals(false, $this->expedition->move($this->robots[0]->getName(), 'L'));
    }

    public function testMultipleMovements() {
        $this->expedition->move($this->robots[0]->getName(), 'FFFFFFFFFRF');
        $expeditionRobots = $this->expedition->getRobots();
        $expectedX = 9;
        $expectedY = 2;
        $expectedDirection = Robot::DIRECTION_SOUTH;

        $this->assertEquals($expectedX, $expeditionRobots[$this->robots[0]->getName()]->getPosX());
        $this->assertEquals($expectedY, $expeditionRobots[$this->robots[0]->getName()]->getPosY());
        $this->assertEquals($expectedDirection, $expeditionRobots[$this->robots[0]->getName()]->getDirection());
    }

    public function testMultipleMovementsOutOfBounds() {

        $this->assertEquals(false, $this->expedition->move($this->robots[0]->getName(), 'FFFFFFFFFF'));
    }

    public function testRockBlokingException() {
        $this->assertEquals(false, $this->expedition->move($this->robots[1]->getName(), 'FLFL'));
    }

}
