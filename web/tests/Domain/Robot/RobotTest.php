<?php

/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests\Domain;

use Tests\TestCaseBase;
use App\Domain\Robot\Robot;

/**
 * Description of RobotTest
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class RobotTest extends TestCaseBase {

    protected $name = 'new Spirit';
    protected $x = 5;
    protected $y = 15;
    protected $direction = Robot::DIRECTION_NORTH;

    /**
     * Test all Getters
     */
    public function testBasicGetters() {

        $robot = new Robot($this->name, $this->x, $this->y, $this->direction);

        $this->assertEquals($this->name, $robot->getName());
        $this->assertEquals($this->y, $robot->getPosY());
        $this->assertEquals($this->x, $robot->getPosX());
        $this->assertEquals($this->direction, $robot->getDirection());
    }

    public function testInvalidPosition() {
        $this->expectException(\App\Domain\Robot\InvalidDirectionException::class);
        $robot = new Robot($this->name, $this->x, $this->y, $this->direction);
        
        $robot->setDirection('INVALID');
    }

}
