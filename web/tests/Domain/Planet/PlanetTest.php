<?php

/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests\Domain;

use Tests\TestCaseBase;
use App\Domain\Planet\Planet;

/**
 * Description of PlanetTest
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class PlanetTest extends TestCaseBase {

    protected $name = 'test';
    protected $x = 100;
    protected $y = 50;
    protected $rocks = [
        ['x' => '15', 'y' => '15'],
        ['x' => '15', 'y' => '16'],
        ['x' => '19', 'y' => '20'],
        ['x' => '20', 'y' => '19'],
    ];


    /**
     * Test all Getters
     */
    public function testBasicGetters() {

        $planet = new Planet($this->name, $this->x, $this->y);

        $this->assertEquals($this->name, $planet->getName());
        $this->assertEquals($this->y, $planet->getY());
        $this->assertEquals($this->x, $planet->getX());
    }

    public function testAddRocks() {
        $planet = new Planet($this->name, $this->x, $this->y);

        foreach ($this->rocks as $rock) {
            $planet->addRock($rock['x'], $rock['y']);
        }
        $this->assertEquals($this->rocks, $planet->getRocks());
    }

    public function testCoordinateHasRock() {
        $planet = new Planet($this->name, $this->x, $this->y);

        foreach ($this->rocks as $rock) {
            $planet->addRock($rock['x'], $rock['y']);
        }

        $this->assertEquals(true, $planet->hasRock($this->rocks[0]['x'], $this->rocks[0]['y']));

        $this->assertEquals(false, $planet->hasRock(0, 0));
    }

    public function testContructorInvalidXSize() {
        $this->expectException(\App\Domain\Planet\InvalidPlanetDimensionsException::class);

        $planet = new Planet($this->name, 0, $this->y);
    }

    public function testContructorInvalidYSize() {
        $this->expectException(\App\Domain\Planet\InvalidPlanetDimensionsException::class);

        $planet = new Planet($this->name, $this->x, 0);
    }

    public function testXCoordinatesOutOfBound() {
        $this->expectException(\App\Domain\Planet\CoordinatesOutOfBoundsException::class);

        $planet = new Planet($this->name, $this->x, $this->y);

        $planet->addRock($this->x + 10, $this->y);
    }

    public function testYCoordinatesOutOfBound() {
        $this->expectException(\App\Domain\Planet\CoordinatesOutOfBoundsException::class);

        $planet = new Planet($this->name, $this->x, $this->y);

        $planet->addRock($this->x, 0);
    }

}
