<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Domain\Expedition;

use App\Domain\Planet\Planet;
use App\Domain\Robot\Robot;
use Ramsey\Uuid\Uuid;

/**
 * Description of Planet
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class Expedition implements \JsonSerializable {

    const MOVE_FORWARD = 'F';
    const MOVE_LEFT = 'L';
    const MOVE_RIGHT = 'R';

    /**
     * Unique identifier
     */
    protected string $uuid;

    /**
     * Handles Expedition's planet
     * @var Planet
     */
    protected Planet $planet;

    /**
     * Can handle one or more robots in robot name => robot object format
     * @var array 
     */
    protected array $robots = [];

    public function __construct(Planet $planet) {
        $this->planet = $planet;
        $this->uuid = Uuid::uuid4()->toString();
    }

    public function getId(): string {
        return $this->uuid;
    }

    public function getRobots(): array {
        return $this->robots;
    }

    public function addRobot(Robot $robot): self {
        if (array_key_exists($robot->getName(), $this->robots)) {
            throw new RobotAlreadyInPlanetException();
        }

        if ($this->planet->hasRock($robot->getPosX(), $robot->getPosY()) || $robot->getPosX() >= $this->planet->getX() || $robot->getPosY() >= $this->planet->getY() || $robot->getPosX() < 0 || $robot->getPosY() < 0) {
            throw new RobotCannotLandInCoordinatesException();
        }

        $this->robots[$robot->getName()] = $robot;

        return $this;
    }

    protected function checkMovementExists(string $direction) {
        if ($direction != self::MOVE_FORWARD && $direction != self::MOVE_LEFT && $direction != self::MOVE_RIGHT) {
            throw new RobotCannotUnderstandException();
        }
    }

    protected function getMovingVector(string $robotName, string $direction): array {

        $movingVector = [
            'x' => $this->robots[$robotName]->getPosX(),
            'y' => $this->robots[$robotName]->getPosY(),
            'direction' => $this->robots[$robotName]->getDirection()
        ];

        switch ($this->robots[$robotName]->getDirection()) {
            case Robot::DIRECTION_NORTH:
                if ($direction === self::MOVE_FORWARD) {
                    $movingVector['y']--;
                } elseif ($direction === self::MOVE_LEFT) {
                    $movingVector['x']--;
                    $movingVector['direction'] = Robot::DIRECTION_WEST;
                } elseif ($direction === self::MOVE_RIGHT) {
                    $movingVector['x']++;
                    $movingVector['direction'] = Robot::DIRECTION_EAST;
                }
                break;
            case Robot::DIRECTION_SOUTH:
                if ($direction === self::MOVE_FORWARD) {
                    $movingVector['y']++;
                } elseif ($direction === self::MOVE_LEFT) {
                    $movingVector['x']++;
                    $movingVector['direction'] = Robot::DIRECTION_EAST;
                } elseif ($direction === self::MOVE_RIGHT) {
                    $movingVector['x']--;
                    $movingVector['direction'] = Robot::DIRECTION_WEST;
                }
                break;
            case Robot::DIRECTION_EAST:
                if ($direction === self::MOVE_FORWARD) {
                    $movingVector['x']++;
                } elseif ($direction === self::MOVE_LEFT) {
                    $movingVector['y']--;
                    $movingVector['direction'] = Robot::DIRECTION_NORTH;
                } elseif ($direction === self::MOVE_RIGHT) {
                    $movingVector['y']++;
                    $movingVector['direction'] = Robot::DIRECTION_SOUTH;
                }

                break;
            case Robot::DIRECTION_WEST:
                if ($direction === self::MOVE_FORWARD) {
                    $movingVector['x']--;
                } elseif ($direction === self::MOVE_LEFT) {
                    $movingVector['y']++;
                    $movingVector['direction'] = Robot::DIRECTION_SOUTH;
                } elseif ($direction === self::MOVE_RIGHT) {
                    $movingVector['y']--;
                    $movingVector['direction'] = Robot::DIRECTION_NORTH;
                }
                break;
        }

        return $movingVector;
    }

    protected function isValidPosition(array $movingVector): bool {


        if ($this->planet->hasRock($movingVector['x'], $movingVector['y']) || $movingVector['x'] >= $this->planet->getX() || $movingVector['y'] >= $this->planet->getY() || $movingVector['x'] < 0 || $movingVector['y'] < 0
        ) {
            return false;
        }

        return true;
    }

    protected function performMovement(string $robotName, array $movingVector) {
        $this->robots[$robotName]
                ->setPosX($movingVector['x'])
                ->setPosY($movingVector['y'])
                ->setDirection($movingVector['direction']);
    }

    public function move(string $robotName, string $directions): bool {

        //foreach movment
        //check if movement exists is ok
        //check if position hasRock (need to get the relation between direction and movement)
        //check if we are in map limit
        //if ok
        //perform movement
        //if KO
        //keep in the same point and throw exception

        if (!array_key_exists($robotName, $this->robots)) {
            throw new RobotDoesNotExistsException();
        }

        $directions = strtoupper($directions);
        for ($i = 0; $i < strlen($directions); ++$i) {
            $this->checkMovementExists($directions[$i]);

            $movingVector = $this->getMovingVector($robotName, $directions[$i]);

            if (!$this->isValidPosition($movingVector)) {
                //throw new RobotHasFoundARockException();
                return false;
            }


            $this->performMovement($robotName, $movingVector);

            
        }
        return true;
    }

    public function getMap() {
        $map = [];
        //set map array
        for ($x = 0; $x < $this->planet->getX(); ++$x) {
            for ($y = 0; $y < $this->planet->getY(); ++$y) {
                $map[$y][$x] = '';
            }
        }

        //set map rocks
        foreach ($this->planet->getRocks() as $rock) {
            $map[$rock['y']][$rock['x']] = 'R';
        }

        return $map;
    }

    public function jsonSerialize() {
        return [
            'expedition_id' => $this->getId(),
            'planet' => $this->planet,
            'robots' => $this->robots,
            'map' => $this->getMap()
        ];
    }

}
