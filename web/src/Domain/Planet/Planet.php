<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Domain\Planet;

/**
 * Description of Planet
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class Planet implements \JsonSerializable {

    /**
     * Handles planet name
     *  
     * @var string 
     */
    protected ?string $name = null;

    /** Handles planet horizontal size
     * @var int 
     */
    protected int $x = 1;

    /** Handles planet vertical sizes
     * @var int 
     */
    protected int $y = 1;

    /** Handlles Rock positions
     * @var array
     */
    protected array $rocks = [];

    /**
     * @param string $name      Name of the planet
     * @param int $x            Horizontal Size
     * @param int $y            Vertical Size
     */
    public function __construct(string $name, int $x, int $y) {

        $this->setName($name)
                ->setX($x)
                ->setY($y);
    }

    /**
     * Returns name fot the planete
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Sets the name for this planet
     *
     * @param string $name      Planet's name
     * @return \self
     */
    public function setName(?string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Gets Horizontal Size for this planet.
     * 
     * @return int
     */
    public function getX(): int {
        return $this->x;
    }

    /**
     * Sets Horizontal size fot this planet
     * 
     * @param int $x
     * @return \self
     * @throw InvalidPlanetDimensionsException
     */
    public function setX(int $x): self {
        if ($x < 1) {
            throw new InvalidPlanetDimensionsException();
        }

        $this->x = $x;
        return $this;
    }

    /**
     * Gets Vertical Size for this planet.
     * 
     * @return int
     */
    public function getY(): int {
        return $this->y;
    }

    /**
     * Sets Vertical size fot this planet
     * 
     * @param int $y
     * @return \self
     * @throw InvalidPlanetDimensionsException
     */
    public function setY(int $y): self {
        if ($y < 1) {
            throw new InvalidPlanetDimensionsException();
        }

        $this->y = $y;
        return $this;
    }

    /**
     * Returns rocks positions
     * @return array
     */
    public function getRocks(): array {
        return $this->rocks;
    }

    /**
     * Adds new Rock
     * 
     * @param int $x    Horizontal coordinate
     * @param int $y    Vertical Coordinate
     * @throw CoordinatesOutOfBoundsException
     * @return \self
     */
    public function addRock(int $x, int $y): self {

        if (!$this->coordinateExists($x, $y)) {
            throw new CoordinatesOutOfBoundsException();
        }

        $this->rocks[] = ['x' => $x, 'y' => $y];
        return $this;
    }

    /**
     * Check if coordinates are inside planet dimensions
     * 
     * @param int $x    Horizontal position
     * @param int $y    Vertical position
     * @return bool     
     */
    public function coordinateExists(int $x, int $y): bool {
        if ($x < 1 || $x > $this->x || $y < 1 || $y > $this->y) {
            return false;
        }

        return true;
    }

    /**
     * Check if there is a rock in this coordinate.
     * 
     * @param int $x    Horizontal position
     * @param int $y    Vertical position
     * @return bool
     */
    public function hasRock(int $x, int $y): bool {
        foreach ($this->rocks as $rock) {
            if ($rock['x'] == $x && $rock['y'] == $y) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove all Rocls from Planet map
     * @return \self
     */
    public function clearAllRocks(): self {
        $this->rocks = [];
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array {

        $return = [
            'name' => $this->getName(),
            'x' => $this->getX(),
            'y' => $this->getY(),
            'rocks' => $this->getRocks()
        ];

        return $return;
    }

}
