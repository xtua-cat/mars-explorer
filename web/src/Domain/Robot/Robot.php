<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Domain\Robot;

/**
 * Description of Planet
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class Robot implements \JsonSerializable {

    CONST DIRECTION_NORTH = 'N';
    CONST DIRECTION_SOUTH = 'S';
    CONST DIRECTION_EAST = 'E';
    CONST DIRECTION_WEST = 'W';

    /** @var string * */
    protected ?string $name = null;

    /** @var int * */
    protected int $posx = 0;

    /** @var int * */
    protected int $posy = 0;

    /** var string * */
    protected ?string $direction = null;

    /**
     * @param string $name      Name of the Robot
     * @param int $x            Horizontal Size
     * @param int $y            Vertical Size
     */
    public function __construct(
            string $name,
            int $x,
            int $y,
            string $direction) {


        $this->validDirections = [
            self::DIRECTION_EAST,
            self::DIRECTION_NORTH,
            self::DIRECTION_SOUTH,
            self::DIRECTION_WEST
        ];

        $this->setName($name)
                ->setPosX($x)
                ->setPosY($y)
                ->setDirection($direction);
    }

    /**
     * Returns Robot's name
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Sets the name for this robot
     *
     * @param string $name      Planet's name
     * @return \self
     */
    public function setName(?string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Gets Horizontal position
     * 
     * @return int
     */
    public function getPosX(): int {
        return $this->posx;
    }

    /**
     * Sets Horizontal position
     * 
     * @param int $x
     * @return \self
     * @throw InvalidPlanetDimensionsException
     */
    public function setPosX(int $x): self {
        $this->posx = $x;
        return $this;
    }

    /**
     * Gets Vertical position
     * 
     * @return int
     */
    public function getPosY(): int {
        return $this->posy;
    }

    /**
     * Sets Vertical position
     * 
     * @param int $y
     * @return \self
     * @throw InvalidPlanetDimensionsException
     */
    public function setPosY(int $y): self {

        $this->posy = $y;
        return $this;
    }

    /**
     * Gets de Robots direction
     * @return string
     */
    public function getDirection(): string {
        return $this->direction;
    }

    /**
     * Sets robots direction
     * @param string $direction
     * @return \self
     * @throws InvalidDirectionException
     */
    public function setDirection(string $direction): self {
        if (!in_array($direction, $this->validDirections)) {
            throw new InvalidDirectionException();
        }

        $this->direction = $direction;
        return $this;
    }

    /**
     * {{@inerithdoc}
     */
    public function jsonSerialize(): array {

        $return = [
            'name' => $this->getName(),
            'posX' => $this->getPosX(),
            'posY' => $this->getPosY(),
            'direction' => $this->getDirection(),
        ];

        return $return;
    }

}
