<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Cache;

use Predis\Client;

/**
 * Description of RedisService
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
final class RedisClient {

    private Client $redis;
    private string $cacheID;

    /**
     * Constructor for RedisClient 
     * 
     * @param Client $redis     Redis Client
     * @param string $cacheID   Unique application key identifier
     */
    public function __construct(
            Client $redis,
            string $cacheID) {

        $this->redis = $redis;
        $this->cacheID = $cacheID;
    }

    /**
     * Appends cacheID in front of key to make it unique for this app.
     * 
     * @param string $key   Element Key
     * @return string       App:element Key
     */
    protected function generateKey(string $key): string {
        return $this->cacheID . ':' . $key;
    }

    /**
     * Check if given key exists in redis cache
     * 
     * @param string $key   Element Key
     * @return bool  
     */
    public function exists(string $key): bool {
        return (bool) $this->redis->exists($this->generateKey($key));
    }

    /**
     * Gets value for a given key
     * 
     * @param string $key       Element Key 
     * @return object|array      JSONdecoded Object
     */
    public function get(string $key, bool $assoc = false) {

        return unserialize(base64_decode($this->redis->get($this->generateKey($key))));
    }

    /**
     * Sets Value for a given Key. Value is stored in JSON format.
     * 
     * @param string $key       Element key
     * @param mixed $value     Object, string integer, it can accept multiple types. 
     * @return self
     */
    public function set(string $key, $value): self {
        $this->redis->set($this->generateKey($key), base64_encode(serialize($value)));
        return $this;
    }

    /**
     * Delete given key from cache
     * 
     * @param string $key
     * @return self
     */
    public function delete(string $key): self {
        $this->redis->del($this->generateKey($key));
        return $this;
    }

}
