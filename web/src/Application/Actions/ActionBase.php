<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\HttpBadRequestException;
use App\Domain\User\UserRepository;
use App\Domain\User\UserRepositoryInterface;
use App\Domain\Issue\IssueRepositoryInterface;
use App\Domain\User\User;
use App\Infrastructure\Persistence\Expedition\RedisExpeditionRepository;
use App\Domain\Expedition\ExpeditionRepositoryInterface;
use App\Application\Settings\SettingsInterface;

/**
 * Action Base Class
 *
 * All actions must extend this class because it contains basic and useful
 * functions that can be reused
 * 
 * @author Xavier Tuà <xtua@xtua.cat>
 */
abstract class ActionBase {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;
    protected SettingsInterface $settings;

    /**
     *
     * @var 
     */
    protected RedisExpeditionRepository $expeditionRepository;

    public function __construct(
            ExpeditionRepositoryInterface $expeditionRepository,
            \App\Application\Settings\SettingsInterface $settings
    ) {
        $this->expeditionRepository = $expeditionRepository;
        $this->settings = $settings;
    }

    public function getExpeditionRepository(): RedisExpeditionRepository {
        return $this->expeditionRepository;
    }

    public function getSettings(): SettingsInterface {
        return $this->settings;
    }

    /**
     * Main function. It is called by default when Slim try to perform an action
     * 
     * @param Request $request
     * @param Response $response
     * @param array $parameters
     * @return Response
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    public function __invoke(
            Request $request,
            Response $response,
            array $parameters): Response {


        $this->request = $request;
        $this->response = $response;
        $this->parameters = $parameters;

        return $this->action();
    }

    /**
     * Try to get request body in JSON format
     * 
     * @return array|object
     * @throws HttpBadRequestException
     */
    protected function getRequestBody() {
        $input = json_decode(file_get_contents('php://input'));

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpBadRequestException(
                    $this->request,
                    'Invalid request Body.'
            );
        }
        if (isset($input->data)) {
            return $input->data;
        }
        return $input;
    }

    /**
     * Try to get Request parameter value.
     * 
     * @param  string $name
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function getParameter(string $name): string {

        //GET
        $allGetVars = $this->request->getQueryParams();
        foreach ($allGetVars as $key => $param) {
            if ($key === $name) {
                return $param;
            }
        }

////POST or PUT
//        $allPostPutVars = $this->request->getParsedBody();
//        foreach ($allPostPutVars as $key => $param) {
//            if ($key === $name) {
//                return $param;
//            }
//        }

        //body
        $body = $this->getRequestBody();

        if (isset($body->$name)) {
            return $body->$name;
        }

        throw new HttpBadRequestException(
                $this->request,
                sprintf("Could not resolve argument '{$name}'.", $name)
        );
    }

    /**
     * Returns Json Encoded API Response
     * 
     * @param ApiPayload $payload
     * @return Response
     */
    protected function respond(ApiPayload $payload): Response {
        $json = json_encode($payload, JSON_PRETTY_PRINT);

        $this->response->getBody()->write($json);
        return $this->response
                        ->withHeader('Content-Type', 'application/json')
                        ->withStatus($payload->getStatusCode());
    }

    /**
     * Contains the main logic. It must be implemented in the extended class
     * 
     * @return Response
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    abstract protected function action(): Response;
}
