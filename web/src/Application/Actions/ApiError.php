<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Actions;

use Ramsey\Uuid\Uuid;

/**
 * This class handles all type of Exceptions to make them compatible with 
 * Api JSON response.
 * 
 * It also adds an unique identifier to each error (UUID) wich can be used for
 * tracking and debugging porpouses.
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class ApiError implements \JsonSerializable {

    /**
     *  Handles Exception information
     * @var Throwable 
     */
    protected \Throwable $exception;

    /**
     * Handles unique identifier for this error
     * @var string
     */
    protected string $uuid;

    /**
     * 
     * @param \Throwable $exception    
     */
    public function __construct(
            \Throwable $exception
    ) {

        $this->exception = $exception;
        $this->uuid = Uuid::uuid4()->toString();
    }

    /**
     * Get Exception element
     * 
     * @return \Throwable
     */
    public function getException(): \Throwable {
        return $this->exception;
    }

    /**
     * Set Exception element
     * 
     * @param \Throwable $exception
     * @return self
     */
    public function setException(\Throwable $exception): self {
        $this->exception = $exception;
        return $this;
    }

    /**
     * Get error unique identifier
     * 
     * @return string
     */
    public function getUUID(): string {
        return $this->uuid;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array {
        return [
            'code' => $this->exception->getCode() ? $this->exception->getCode() : 500,
            'uuid' => $this->getUUID(),
            'title' => $this->exception->getMessage(),
            'detail' => $this->exception->getTraceAsString(),
        ];
    }

}
