<?php

declare(strict_types=1);

/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Actions\Expedition;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\ActionBase;
use Slim\Exception\HttpNotImplementedException;
use App\Application\Actions\ApiPayload;
use App\Domain\Issue\Issue;
use App\Domain\User\UserStackOverflow;
use App\Domain\Expedition\RobotHasFoundARockException;

/**
 * Description of IssueJoinAction
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class ExpeditionMoveAction extends ActionBase {

    /**
     * {@inheritdoc}
     */
    protected function action(): Response {

        $expeditionId = $this->getParameter('expedition_id');
        $robotName = $this->getParameter('robot_name');
        $directions = $this->getParameter('directions');

        /** Validations * */
        if (!$expedition = $this->getExpeditionRepository()->findById($expeditionId)) {
            throw new ExpeditionNotFoundException($this->request);
        }


        $result = $expedition->move($robotName, $directions);

        //save config to redis persistance
        $this->getExpeditionRepository()
                ->persist($expedition)
                ->flush();
        if (!$result) {
            throw new RobotHasFoundARockException();
        }

        return $this->respond(new ApiPayload(200, $expedition));
    }

}
