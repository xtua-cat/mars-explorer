<?php

declare(strict_types=1);

/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Actions\Expedition;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\ActionBase;
use Slim\Exception\HttpNotImplementedException;
use App\Application\Actions\ApiPayload;
use App\Domain\Issue\IssueNotFoundException;
use App\Domain\Planet\Planet;
use App\Domain\Expedition\Expedition;
use App\Domain\Robot\Robot;

/**
 * Description of IssueGetVotes
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class ExpeditionNewAction extends ActionBase {

    protected function action(): Response {

        $robotName = $this->getParameter('robot_name');
        $startX = (int) $this->getParameter('x');
        $startY = (int) $this->getParameter('y');
        $startDirection = strtoupper($this->getParameter('direction'));


        //create map, robot and expedition based on settings and user parameters
        $marsConfig = $this->getSettings()->get('planets')['mars'];

        $planet = new Planet('mars', (int) $marsConfig['x'], (int) $marsConfig['y']);
        foreach ($marsConfig['rocks'] as $x => $y) {
            $planet->addRock($x, $y)
                    ->addRock($y, $x);
        }
        $robot = new Robot($robotName, $startX, $startY, $startDirection);

        $expedition = new \App\Domain\Expedition\Expedition($planet);
        $expedition->addRobot($robot);


        //save config to redis persistance
        $this->getExpeditionRepository()
                ->persist($expedition)
                ->flush();

        return $this->respond(new ApiPayload(200, $expedition));
    }

}
