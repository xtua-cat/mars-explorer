<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Application\Handlers;

use App\Application\Actions\ApiError;
use App\Application\Actions\ApiPayload;
use Slim\Handlers\ErrorHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpException;

/**
 * Description of ExceptionHandler
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class ExceptionHandler extends ErrorHandler {

    /**
     * @inheritdoc
     */
    protected function respond(): Response {

        $exception = $this->exception;

        $apiError = new ApiError($exception);
        if ($exception instanceof HttpException) {
            $payload = new ApiPayload($exception->getCode(), null, $apiError);
        } else {
            $payload = new ApiPayload(500, null, $apiError);
        }

        $response = $this->responseFactory->createResponse($payload->getStatusCode());
        $response->getBody()->write(json_encode($payload));
        return $response->withHeader('Content-Type', 'application/json');
    }

}
