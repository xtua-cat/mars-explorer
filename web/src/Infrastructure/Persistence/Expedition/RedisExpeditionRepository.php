<?php

declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 Xavier Tuà <xtua@xtua.cat>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App\Infrastructure\Persistence\Expedition;

use App\Domain\Issue\Issue;
use App\Domain\Issue\IssueRepositoryInterface;
use App\Infrastructure\Persistence\RedisRepositoryBase;
use App\Application\Cache\RedisClient;
use App\Domain\Expedition\ExpeditionRepositoryInterface;
use App\Domain\Expedition\Expedition;

/**
 * Description of RedisExpeditionRepository
 *
 * @author Xavier Tuà <xtua@xtua.cat>
 */
class RedisExpeditionRepository extends RedisRepositoryBase implements ExpeditionRepositoryInterface {

    private ?array $expeditions = [];

    public function __construct(RedisClient $client, array $expeditions = []) {

        parent::__construct('expeditions', $client);
        $this->expeditions = $expeditions;


        if ($this->redis->exists($this->cacheId)) {
            $this->expeditions = $this->redis->get($this->cacheId);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array {
        return array_values($this->expeditions);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(string $id): ?Expedition {
        if (!isset($this->expeditions[$id])) {
            return null;
        }

        return $this->expeditions[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function persist(Expedition $expedition): self {

        $this->expeditions[$expedition->getId()] = $expedition;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function flush(): self {
        $this->redis->set($this->cacheId, $this->expeditions);
        return $this;
    }

}
