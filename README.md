# Mars-Explorer 
For full documentation with demo images, please open README.docx.

# Mars Rover Mission Specifications

## Your Task

You&#39;re part of the team that explores Mars by sending remotely controlled vehicles to the surface of the planet. Develop a software that translates the commands sent from earth to instructions that are understood by the rover.

##

## Requirements

- You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W)

it is facing.

- The rover receives a collection of commands. (E.g.) FFRRFFFRL
- The rover can move forward (f).
- The rover can move left/right (l,r).
- Suppose we are on a really weird planet that is square. 200x200 for example :)
- Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point, aborts the sequence and reports the obstacle.

## Take into account

- Rovers are expensive, make sure the software works as expected.

# Design

When designing this project, we keep in mind some basic points:

- **We expect our code to be reused** (to explore other planets or satellites).

- **We also expect that the logic must be separated from objects itself** (Maybe in nearly future, robots can go backwards, and we do not want to rewrite all code. It is preferred to keep moving orders apart of robots&#39; core components.).
- **Unit testing** to be sure all functions work as expected (Robots are so expensive).
- **Finally, we will also do a basic functional test to validate all gears works together as expected.** This will be done in a web prototype (We will see later why it is a prototype and not a main development).

We will use Docker with two environments, one with PHP and Apache for the API/Backoffice, and another one for Redis.

# Backoffice

### Technological stack

Because it is an open project, we will use:

- **Slim frameworks v.4** with those dependencies:
  - predis/predis: PHP redis client
  - php-di/php-di: Dependency injector to make our development easier and avoid errors.
- **PHP 7.4.**
- **JsonAPI** : as a basic communication specification between front and back. We all will understand how to send and receive information [jsonapi v1.0 (last stable)] ([https://jsonapi.org/format/](https://jsonapi.org/format/)) .
- **Redis as an in memory data storage** so we will be able to move our robot from different devices.

## Code Structure

Following the rules we explained in the design paragraph, we will work with 3 basic classes:

1. **Planet:** This class will contain all information related to our planet (dimensions, name, etc). Mars will implement or extend this class.
2. **Robot:** It will contain all information related to the robot (direction, position, etc)
  1. Direction
  2. Position
3. **Expedition:** It will contain all logic to move one or more robot in any planet.
  1. Planet
  2. Robot
  3. All logic and validation

Moreover, we will develop a global error handler and unit test cases.

# Frontoffice prototype

This front office will be just a basic demonstration to show how the APP works. We will use HTML + jQuery + Bootstrap framework.

A better way to do this, would be to create a Javascript class to manage communication (sockets would be much better).

Vue or NodeJS would be much better.

## Demo Images

![](RackMultipart20210617-4-1mfvvmn_html_af8a3461f2946636.png)

![](RackMultipart20210617-4-1mfvvmn_html_c457b4c31e2ea9e3.png)

![](RackMultipart20210617-4-1mfvvmn_html_436b3c8e0f7d3623.png)

![](RackMultipart20210617-4-1mfvvmn_html_26ffa227c1b1215f.png)
